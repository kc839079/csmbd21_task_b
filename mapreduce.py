import multiprocessing as mp

#Define simple function for MapReduce data flow (Mapping -> Shuffling -> Reducing)
def map_func(map_in):
    """simple map function: assign value to key"""
    key, value = map_in
    value = 1
    return (key, value)

def shuffle_func(map_out):
    """sorting map output by key"""
    reduce_in = {}
    for key, value in map_out:
        if key not in reduce_in:
            reduce_in[key] = [value]
        else:
            reduce_in[key].append(value)
    return reduce_in

def reduce_func(reduce_in):
    """sum the value for each key"""
    key, value_list = reduce_in
    return (key, sum(value_list))

#MapReduce Process with multithreading application
def MT_MapReduce(input):
    """MapReduce process with multithreading application"""
    with mp.Pool(processes = mp.cpu_count()) as pool:
        #assign map_func method to the partitioned input and store as 'map_out'
        map_out = pool.map(map_func, input, chunksize = int(len(input)/mp.cpu_count()))
        # print(map_out)

        #call shuffle_func method using 'map_out' and store as 'reduce_in'
        reduce_in = shuffle_func(map_out)
        # print(reduce_in)

        #call reduce_func method using 'reduce_in' and store as 'reduce_out'
        reduce_out = pool.map(reduce_func, reduce_in.items() , chunksize =int(len(reduce_in.keys())/mp.cpu_count()))
        #convert list to dictionary for further usage
        reduce_out = dict(reduce_out)
        # print(reduce_out)
    return reduce_out
