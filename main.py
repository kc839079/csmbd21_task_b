from PASSENGER import PASSENGER

#assign variables for file path and data headers
file_path = "DATA/AComp_Passenger_data_no_error.csv"
colnames = ["passenger_id", "flight_id", "departure_code", "arrival_code", "departure_time", "flight_time"]

if __name__ == "__main__":
    #call the PASSENGER class with previously assigned variables
    passenger = PASSENGER(file_path, colnames)
    #call max_flight method on the class to obtain the passenger with highest number of flight
    passenger.max_flight()