import pandas as pd
from mapreduce import MT_MapReduce

#Generate class for reusability and further development reasons
class PASSENGER:
    def __init__(self, file_path, colnames):
        #assign parameter to get the data from file
        self.file_path = file_path
        self.colnames = colnames
        self.df = pd.read_csv(file_path, names = colnames)
       
        #call get_map_in method using dataset 'df' to obtain map input ('map_in')
        self.map_in = self.get_map_in(self.df)

        #call MT_MapReduce method using map input 'map_in' to obtain MapReduce result
        #the result from the input is flight count by passenger id ('flight_count')
        self.flight_count = MT_MapReduce(self.map_in)
        
        #export MapReduce output as CSV file
        self.map_out = pd.DataFrame([self.flight_count]).transpose()
        self.map_out.to_csv(r'reducer_output.csv', header = False)

    def get_map_in(self, data):
        """retrieving key that associated with value to be used as map input"""
        #specify key and value for obtaining input
        map_in = []
        key_list = data["passenger_id"]
        value_list = data["flight_id"]

        #iterate rows of input key and value and return as list of tuples (key, value)
        for passenger, flight in zip(key_list, value_list):
            map_in.append((passenger, flight))
        return map_in
    
    def max_flight(self):
        """objective function"""
        #obtain passenger ID with highest number of flight
        passenger_max = max(self.flight_count, key = self.flight_count.get)
        flight_max = self.flight_count[passenger_max]
        print(f"Passenger [{passenger_max}] has the highest number of flight at {flight_max} flights")
        